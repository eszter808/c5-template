<?php
defined('C5_EXECUTE') or die("Access Denied.");

$c = Page::getCurrentPage();

/** @var \Concrete\Core\Utility\Service\Text $th */
$th = Core::make('helper/text');
/** @var \Concrete\Core\Localization\Service\Date $dh */
$dh = Core::make('helper/date');
$d = Core::make('date');

if ($c->isEditMode() && $controller->isBlockEmpty()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.') ?></div>
    <?php
} else {
    ?>
    <div class="p-news__wrapper">
	    <?php if (isset($pageListTitle) && $pageListTitle): ?>
	    	<h1><?php echo h($pageListTitle) ?></h1>
		<?php endif; ?>
		<ul class="p-news-list">
			<?php foreach ($pages as $page):

				$title = $th->entities($page->getCollectionName());
				$url = ($page->getCollectionPointerExternalLink() != '') ? $page->getCollectionPointerExternalLink() : $nh->getLinkToCollection($page);
				$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
				$target = empty($target) ? '_self' : $target;
				$date = $d->formatCustom('Y年m月j日', $page->getCollectionDatePublic());
				$category = $page->getAttribute('tags');
			?>
			<li class="p-news-list__content">
				<time class="p-news-list__date"><?php echo $date?></time>
				<span class="c-label c-label--primary"><?php echo $category?></span>
				<a  class="p-news-list__link" href="<?php echo $url ?>" target="<?php echo $target ?>"><?php echo $title ?></a>
			</li>
			<?php endforeach; ?>
		</ul>
    </div><!-- END news-wrap -->
    <?php
} ?>
