<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php $this->inc('elements/header.php'); ?>
<main class="main-wrap l-main">
<h1 class="page-title p-page-title"><?php echo $c->getCollectionName() ?></h1>
<div class="container">
	<?php
		/*
		 * パンくずを表示
		 * autonav ブロックを呼び出して表示する
		 */
		$bt = BlockType::getByHandle('autonav');
		$bt->controller->orderBy = 'display_asc';
		$bt->controller->displayPages = 'top';
		$bt->controller->displaySubPages = 'relevant_breadcrumb';
		$bt->controller->displaySubPageLevels = 'all';
		$bt->render('templates/breadcrumb');
	?>
</div>
<div class="breadcrumbs hidden-xs">
	<div class="container">
	<?php
	$a = new GlobalArea('Breadcrumbs');
	//$a->disableControls();
	$a->display($c);
	?>
	</div>
</div>
<div class="container maincontents">
	<div class="row">
		<div class="col-xs-12 col-sm-9">
			<article class="main">
			<?php
			$a = new Area('Main');
			$a->setAreaGridMaximumColumns(12);
			$a->display($c);
			?>
			</article>
		</div>
		<div class="col-xs-12 col-sm-3">
			<div id="side">
				<?php
					$a = new GlobalArea('Sidenav');
					$a->display($c);
				?>
			</div>
		</div>
	</div>
</div>

</main>
<?php $this->inc('elements/footer.php'); ?>
