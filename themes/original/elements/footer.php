<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<footer class="l-footer">
	<div class="l-footer__head">
		<div class="p-pagetop">
			<div class="fa fa-chevron-up"></div><a href="#">page top</a>
		</div>
	</div>
    <div class="container">
		<div class="l-footer__body">
		    <div class="l-footer__info">
			    <?php
				    $a = new GlobalArea('Footer Information');
				    $a->display();
				?>
		    </div>
		    <div class="l-footer__nav">
			    <?php
				    $a = new GlobalArea('Footer Navigation');
				    $a->display();
				?>
			    <div class="l-footer__utility">
				    <?php
					    $a = new GlobalArea('Footer Utility');
					    $a->display();
					?>
			    </div>
		    </div>
    	</div>
    </div><!-- END container -->
	<div class="l-footer__copyright c-copyright">&copy;2018 concrete7 Inc.</div>
</footer><!-- END l-footer -->
</div><!-- END l-wrapper -->
</div><!-- END ccm-page -->
<script src="<?php echo $view->getThemePath() ?>/assets/js/bootstrap.js"></script>
<script src="<?php echo $view->getThemePath() ?>/assets/js/bundle/bundle.js"></script>
<script src="<?php echo $view->getThemePath() ?>/assets/js/common.js"></script>
<?php View::element('footer_required'); ?>
</body>
</html>