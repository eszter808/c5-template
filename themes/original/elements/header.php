<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage() ?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script type="text/javascript">
	if ((navigator.userAgent.indexOf('iPhone') > 0) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
	        //iPhone、iPod、Androidの設定
	        document.write('<meta name="viewport" content="width=device-width,initial-scale=1">');
	    }else{
	        //それ以外（PC、iPadなど）の設定
	        document.write('');
	    }
	</script>
	<meta name="format-detection" content="telephone=no">
    <?php View::element('header_required'); ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/earlyaccess/mplus1p.css">
    <link rel="stylesheet" href="<?php echo $view->getThemePath() ?>/assets/css/vendor/bootstrap/bootstrap.css">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $view->getThemePath() ?>/assets/css/common.css">
    <link rel="stylesheet" href="<?php echo $view->getThemePath() ?>/assets/css/vendor/vendor.css">
</head>
<body>

<div class="<?php echo $c->getPageWrapperClass() ?>">

<div class="l-wrapper">
	<header class="l-header">
		<div class="l-header__inner container">
	        <div class="p-header-logo">
	            <?php
		            $a = new GlobalArea('Header Site Title');
		            $a->display();
		        ?>
	        </div>
	        <div class="p-headerNav">
	        	<div class="p-headerNav__utility">
	                <?php
		                $a = new GlobalArea('Utility Navigation');
		                $a->display();
		            ?>
	        	</div><!-- END p-headerNav__utility -->
	        	<div class="p-headerNav__global p-headerNav_spMenu">
	                <?php
		                $a = new GlobalArea('Header Navigation');
		                $a->display();
		            ?>
	        	</div><!-- END p-headerNav__global" -->
	        </div><!-- END p-headerNav -->
		</div><!-- END header__inner -->
	</header><!-- END l-header -->
