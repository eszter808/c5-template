<?php defined('C5_EXECUTE') or die("Access Denied.");
/*
 * 親情報取得
 */
$parent = Page::getByID($c->getCollectionParentID());
$this->inc('elements/header.php'); ?>

<main class="main-wrap l-main">
<h1 class="page-title p-page-title"><?php echo $parent->getCollectionName() ?></h1>
<div class="container hidden-xs">
	<?php
		/*
		 * パンくずを表示
		 * autonav ブロックを呼び出して表示する
		 */
		$bt = BlockType::getByHandle('autonav');
		$bt->controller->orderBy = 'display_asc';
		$bt->controller->displayPages = 'top';
		$bt->controller->displaySubPages = 'relevant_breadcrumb';
		$bt->controller->displaySubPageLevels = 'all';
		$bt->render('templates/breadcrumb');
	?>
</div>
<div class="container">
    <div class="row">
		<div class="col-xs-12 col-sm-9">
			<h2 class="article-title">
				<span class="title"><?php echo $c->getCollectionName() ?></span>
				<span class="date"><?php echo $c->getCollectionDatePublicObject()->format('Y.m.d');?></span>
			</h2>
			<article class="main">
			<?php
			$a = new Area('Main');
			$a->setAreaGridMaximumColumns(12);
			$a->display($c);
			?>
			</article>
		</div>
		<div class="col-xs-12 col-sm-3">
			<div id="side">
				<?php
					$a = new GlobalArea('Sidenav');
					$a->display($c);
				?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<?php
			$a = new GlobalArea('topics-footer');
			$a->setAreaGridMaximumColumns(12);
			$a->display($c);
			?>
		</div>
	</div>
</div>
</main>
<?php $this->inc('elements/footer.php'); ?>
