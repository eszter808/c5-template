<?php
namespace Application\Theme\Original;

class PageTheme extends \Concrete\Core\Page\Theme\Theme
{
  
    public function getThemeName()
    {
        return t('original'); // テーマ名
    }

    public function getThemeDescription()
    {
        return t('original'); // テーマの説明
    }

    protected $pThemeGridFrameworkHandle = 'bootstrap3';

    public function registerAssets()
    {
        $this->providesAsset('javascript', 'bootstrap/*');
        $this->providesAsset('css', 'bootstrap/*');
        $this->providesAsset('css', 'core/frontend/*');
        $this->providesAsset('css', 'blocks/page_list');
        $this->providesAsset('css', 'blocks/feature');
        $this->providesAsset('css', 'blocks/social_links');

        $this->requireAsset('css', 'font-awesome');
        $this->requireAsset('javascript', 'jquery');
        $this->requireAsset('javascript', 'picturefill');
        $this->requireAsset('javascript-conditional', 'html5-shiv');
        $this->requireAsset('javascript-conditional', 'respond');
    }

    public function getThemeEditorClasses()
    {
        return [
            ['title' => t('Span'), 'spanClass' => '', 'forceBlock' => '-1'],
            ['title' => t('Standard Button'), 'spanClass' => 'btn btn-default', 'forceBlock' => '-1'],
            ['title' => t('Success Button'), 'spanClass' => 'btn btn-success', 'forceBlock' => '-1'],
            ['title' => t('Primary Button'), 'spanClass' => 'btn btn-primary', 'forceBlock' => '-1'],
            // Block-level styles
			['title' => t('Level1'), 'element' => 'h1', 'spanClass' => 'c-title--level1'],
			['title' => t('Level2'), 'element' => 'h2', 'spanClass' => 'c-title--level2'],
			['title' => t('Level3'), 'element' => 'h3', 'spanClass' => 'c-title--level3'],
			['title' => t('Level4'), 'element' => 'h4', 'spanClass' => 'c-title--level4'],
			['title' => t('Level5'), 'element' => 'h5', 'spanClass' => 'c-title--level5'],
			['title' => t('Level6'), 'element' => 'h6', 'spanClass' => 'c-title--level6'],
        ];
    }
}