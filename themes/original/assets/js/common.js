// page top scroll
$(function() {
    var topBtn = $('.p-pagetop');    
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});


// PCでマウスホイールでのスクロール操作の制御
$(function(){
	
	var state = false;
	var scrollpos;
	
	$('.p-responsive-nav__icon').on('click', function(){
		if (state == false ) {
			$("body").addClass('noScroll');
		    $(window).on('wheel',function(e){
		      e.preventDefault();
		    });
		    $(window).on('touchmove.noScroll',function(e){
		      e.preventDefault();
		    });
			$("body").css('overflow','hidden');
			state = true;
		} else {
			$("body").removeClass('noScroll');
			$(window).off('wheel');
			$(window).off('.noScroll');
			$("body").css('overflow','auto');
			state = false;
		}
	}) 
});

// js-heightline の高さを揃える
$(".js-heightline").heightLine();