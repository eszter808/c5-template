var baseW = 1068;	//基準となるブレークポイント
var iOSviewportW = 0;
var ua = navigator.userAgent.toLowerCase();
var isiOS = (ua.indexOf("iphone") > -1) || (ua.indexOf("ipod") > -1) || (ua.indexOf("ipad") > -1);
if(isiOS){
	iOSviewportW = document.documentElement.clientWidth;
}
function updateMetaViewport(){
	var viewportContent;
	var w = window.outerWidth;
	var elm = document.querySelector("meta[name='viewport']");
	if(isiOS){
		w = iOSviewportW;
	}
	if(w < baseW){
		viewportContent = "width="+baseW+", initial-scale=1";
		elm.parentNode.removeChild(elm);
	}else{
		viewportContent = "width=device-width, initial-scale=1";
		document.querySelector("meta[name='viewport']").setAttribute("content", viewportContent);
	}
}
//イベントハンドラ登録
window.addEventListener("resize", updateMetaViewport, false);
window.addEventListener("orientationchange", updateMetaViewport, false);
//初回イベント強制発動
var ev = document.createEvent("UIEvent");
ev.initEvent("resize", true, true)
window.dispatchEvent(ev);