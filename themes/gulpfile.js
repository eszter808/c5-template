var gulp = require('gulp');
// Utility
var del = require('del');
var copy = require('gulp-copy');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var rimraf = require('rimraf');
var plumber = require('gulp-plumber');
var data = require('gulp-data');
var minifyejs = require('gulp-minify-ejs');
var gulpif = require('gulp-if');
var imagemin = require('gulp-imagemin');
// Sass
var sass = require('gulp-sass');
var sassGlob = require("gulp-sass-glob");
var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var autoprefixer = require('gulp-autoprefixer');
var mqpacker = require('css-mqpacker');
var csswring = require('csswring');
var gulpif = require('gulp-if');
var svgSprite = require('gulp-svg-sprites');
var options = require('minimist')(process.argv.slice(2));
var watch = require('gulp-watch');
var runSequence = require('run-sequence');
var csscomb = require('gulp-csscomb');
var cleanCss = require('gulp-clean-css');
// browser-sync
var browserSync = require('browser-sync');
var envOption = { string: 'env' };
var options = require('minimist')(process.argv.slice(2), envOption);
var isProduction = (options.env === 'production') ? true : false;

// コンフィグファイルの読み込み
var gulpconfig = require('./gulpconfig.json');

/**
 * 開発用のデベロップパス。ディレクトリ名はプロジェクトにあわせて変更します。
 */
var config = {
  'build': gulpconfig.theme,
  'src': 'src',
  'scss': 'src/assets/css',
  'sprite': 'src/assets/sprite',
  'styleguide': 'src/assets/styleguide',
  'minifyCss': 'src/assets/css/*.scss',
  'js': ['src/**/*.js', '!' + 'src/assets/js/bundle/**/*.js'],
  'bundleJs': 'src/assets/js/bundle/**/*.js',
  'php': 'src/**/*.p',
  'fonts': 'src/assets/**/*.{eot,svg,ttf,woff,woff2}',
};

/**
 * 公開用のリリースパス。ディレクトリ名はプロジェクトにあわせて変更します。
 */
var release = {
  'root': gulpconfig.theme,
  'html': gulpconfig.theme,
  'minifyCss': gulpconfig.theme + '/assets/css/',
  'bundleJs': gulpconfig.theme + '/assets/js/bundle/',
  'styleguide': gulpconfig.theme + '/assets/styleguide/',
}

var AUTOPREFIXER_BROWSERS = [
  // @see https://github.com/ai/browserslist#browsers
  // Major Browsers（主要なブラウザの指定）
  'last 2 version', // （Major Browsersの）最新2バージョン
  // 'Chrome >= 34', // Google Chrome34以上
  // 'Firefox >= 30', // Firefox30以上
  'ie >= 9', // IE9以上
  // 'Edge >= 12', // Edge12以上
  'iOS >= 7', // iOS7以上
  // 'Opera >= 23', // Opera23以上
  // 'Safari >= 7', // Safari7以上

  // Other（Androidなどのマイナーなデバイスの指定）
  'Android >= 4.0' // Android4.0以上
];

/*
 * データのコピー
 */
gulp.task('copy', function () {
    return gulp.src([config.src + '/**/*.js', '!' + config.src + '/**/*.{eot,svg,ttf,woff,woff2}'])
        .pipe(gulp.dest(config.build));
});


/**
 * デベロップディレクトリの画像を圧縮、
 * 階層構造を維持したまま、リリースディレクトリに出力します。
 */
gulp.task('image', function() {
  return gulp.src(config.src + '/**/*.{png,jpg,gif,svg}')
  .pipe(imagemin({
    // jpgをロスレス圧縮（画質を落とさず、メタデータを削除）。
    progressive: true,
    // gifをインターレースgifにします。
    interlaced: true,
    // PNGファイルの圧縮率（7が最高）を指定します。
    optimizationLevel: 7
  }))
  .pipe(gulp.dest(release.root))
  .pipe(browserSync.reload({stream: true}));
});


/*
 * sassコンパイル
 */
gulp.task('sass', function () {
    return gulp.src(config.src + '/**/*.scss')
		.pipe(sourcemaps.init())
        .pipe(plumber({
            errorHandler: function(err) {
                console.log(err.messageFormatted);
                this.emit('end');
            }
        }))
        .pipe(sassGlob())
        .pipe(sass({ outputStyle: 'expanded' }))
		.pipe(autoprefixer({
		browsers: AUTOPREFIXER_BROWSERS,
		}))
		.pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.build))
		.pipe(browserSync.reload({stream: true}));
});

/*
 * `sass`タスクにミニファイとリネームを追加します。
 */
gulp.task('minifyCss', function () {
    return gulp.src(config.src + '/**/*.scss')
		.pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(sass({ outputStyle: 'expanded' }))
        .pipe(postcss([
            autoprefixer({
                browsers: AUTOPREFIXER_BROWSERS,
            })
        ]))
		.pipe(csscomb())
        .pipe(gulpif(isProduction, postcss([
            mqpacker({
                sort: function (a, b) {
                    return b.localeCompare(a);
                }
            }),
            csswring()
        ])))
		.pipe(rename({suffix: '.min'}))
		.pipe(cleanCss())
		.pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.build))
});

/**
 * デフォルトjsファイルとjQueryをリリースディレクトリに出力します。
 */
gulp.task('js', function() {
  return gulp.src(config.js, {base: config.src})
  .pipe(gulp.dest(release.root))
  .pipe(browserSync.reload({stream: true}));
});

/**
 * phpファイルをリリースディレクトリに出力します。
 */
gulp.task('php', function() {
  return gulp.src(config.php, {base: config.src})
  .pipe(rename({extname: '.php'}))
  .pipe(gulp.dest(release.root))
  .pipe(browserSync.reload({stream: true}));
});

/**
 * デフォルトjsファイルとjQueryをリリースディレクトリに出力します。
 */
gulp.task('fonts', function() {
  return gulp.src(config.fonts, {base: config.src})
  .pipe(gulp.dest(release.root))
  .pipe(browserSync.reload({stream: true}));
});

/**
 * デベロップディレクトリにあるjQueryプラグインなどのファイルを連結してリリースディレクトリに出力します。
 */
gulp.task('bundleJs', function() {
  return gulp.src(config.bundleJs)
  .pipe(sourcemaps.init())
  .pipe(concat('bundle.js'))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(release.bundleJs))
  .pipe(browserSync.reload({stream: true}));
});

/**
 * リリースディレクトリを削除します。
 */
gulp.task('clean', function (cb) {
  rimraf(release.root, cb);
});


/**
 * ローカルサーバーを起動します。
 */
gulp.task('browser-sync', function() {
  browserSync.init({
    proxy:'localhost:80/' + gulpconfig.project + '/cms/',
	open: "external"
  });
});


/**
 * ローカルサーバーを起動します。
 */
gulp.task('browser-reload', function() {
    browserSync.reload();
});


/**
 * watchタスクを指定します。
 */
gulp.task('watch',['browser-sync','build'],function () {
    // scssファイルの変更を監視しsassタスクを実行する。
    watch([config.src + '/**/*.scss'], function () {
        gulp.start(['sass']);
    });

    // cssファイルの変更を監視しcssタスクを実行する。
    watch([config.build + '/**/*.css'], function () {
        gulp.start(['browser-reload']);
    });

    // phpファイルの変更を監視しタスクを実行する。
    watch([config.php], function () {
        gulp.start(['php']);
    });

    // phpファイルの変更を監視しphpタスクを実行する。
    watch([config.build + '/**/*.php'], function () {
        gulp.start(['browser-reload']);
    });

    // jsファイルの変更を監視しタスクを実行する。
    watch(config.js, function () {
        gulp.start(['js']);
    });

    // jsファイルの変更を監視しjsタスクを実行する。
    watch([config.build + '/**/*.js'], function () {
        gulp.start(['browser-reload']);
    });

    // imageファイルの変更を監視しタスクを実行する。
    watch(config.src + '/**/*.{png,jpg,gif,svg}', function () {
        gulp.start(['image']);
    });
});


/**
 * 開発に使用するタスクです。
 * `gulp`タスクにbrowser-syncを追加します。
 * ローカルサーバーを起動し、リアルタイムに更新を反映させます。
 */
gulp.task('default', ['clean'], function() {
  runSequence(  'watch'  );
});

/**
 * 一連のタスクを処理します（画像の圧縮は`release`タスクでおこないます）。
 */
gulp.task('build', [/*'ejs',*/ 'sass', 'js', 'php', 'fonts', 'bundleJs', 'image']);

/**
 * リリースに使用するタスクです。
 * リリースディレクトリを最新の状態にしてから、ファイルの圧縮をします。
 */
gulp.task('release', ['clean'], function() {
  runSequence(
    [/*'ejs',*/ 'sass', 'minifyCss', 'js', 'php', 'fonts', 'bundleJs', 'image']
  )
});

